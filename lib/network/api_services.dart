import 'package:dio/dio.dart';
import 'package:gharelu/network/api_url.dart';

class ApiServices {
  Future<dynamic> get(url) async {
    var dio = Dio();
    dio.interceptors.add(
        LogInterceptor(responseBody: true, request: true, requestBody: true));

    dio.options.headers['content-Type'] = 'application/json';
    // dio.options.headers["Authorization"] = "Bearer ${token}";
    //Add bearer token in above line if available.

    try {
      var response = await dio.get(ApiUrl.baseUrl + url);
      return response.data;
    } on DioError catch (e) {
      return e;
    }
  }

  Future<dynamic> post(url, data) async {
    BaseOptions options = new BaseOptions(
      // baseUrl: ApiUrl.baseUrl,
      connectTimeout: 5000,
      receiveTimeout: 3000,
      // contentType:Headers.formUrlEncodedContentType
    );
    var dio = Dio();
    // dio.options.contentType= Headers.formUrlEncodedContentType;

    // dio.options.contentType = Headers.formUrlEncodedContentType;
    // dio.options.headers['content-Type'] = 'application/json';
    // dio.options.headers["Authorization"] = "Bearer ${token}";

    dio.interceptors.add(
        LogInterceptor(responseBody: true, request: true, requestBody: true));

    try {
      var response = await dio.post(ApiUrl.baseUrl + url,
          data: data,
          options: Options(
              contentType: Headers.jsonContentType,
              headers: {"Authorization": "Bearer "}));

      return response.data;
    } on DioError catch (e) {
      return e;
    }
  }

  Future<dynamic> put(url, data) async {
    BaseOptions options = new BaseOptions(
      // baseUrl: ApiUrl.baseUrl,
      connectTimeout: 5000,
      receiveTimeout: 3000,
      // contentType:Headers.formUrlEncodedContentType
    );
    var dio = Dio();
    // dio.options.contentType= Headers.formUrlEncodedContentType;

    // dio.options.contentType = Headers.formUrlEncodedContentType;
    // dio.options.headers['content-Type'] = 'application/json';
    // dio.options.headers["Authorization"] = "Bearer ${token}";

    dio.interceptors.add(
        LogInterceptor(responseBody: true, request: true, requestBody: true));

    try {
      var response = await dio.put(ApiUrl.baseUrl + url,
          data: data,
          options: Options(
              contentType: Headers.jsonContentType,
              headers: {"Authorization": "Bearer "}));

      return response.data;
    } on DioError catch (e) {
      return e;
    }
  }
}
