import 'package:flutter/material.dart';

class AppColors {
  static const primaryColor = Color(0xFF27A939);
  static const accentColor = Color(0xFFFFFFFF);

  static const lightGray = Color(0xFFF0F0F0);

  static const gray = Color(0xFFADADAD);
  static const darkGray = Color(0xFF626262);
  static const mediumGray = Color(0xFF9C9C9C);
}
