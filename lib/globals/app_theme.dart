import 'package:flutter/material.dart';
import 'package:gharelu/globals/app_colors.dart';

const MaterialColor kPrimaryColor = MaterialColor(
  0xFF6C65D1,
  <int, Color>{
    50: Color(0xFF27A939),
    100: Color(0xFF27A939),
    200: Color(0xFF27A939),
    300: Color(0xFF27A939),
    400: Color(0xFF27A939),
    500: Color(0xFF27A939),
    600: Color(0xFF27A939),
    700: Color(0xFF27A939),
    800: Color(0xFF27A939),
    900: Color(0xFF27A939),
  },
);
final appTheme = ThemeData(
  scaffoldBackgroundColor: Colors.white,
  // Define the default brightness and colors.
  brightness: Brightness.light,
  primaryColor: AppColors.primaryColor,
  platform: TargetPlatform.iOS,
  // Define the default font family.
  fontFamily: 'Lato',
  appBarTheme: const AppBarTheme(
    centerTitle: true,
    elevation: 0,
    iconTheme: IconThemeData(color: AppColors.primaryColor, size: 10),
    backgroundColor: AppColors.primaryColor,
    titleTextStyle: TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w700,
      fontSize: 18,
    ),
    toolbarTextStyle: TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w700,
      fontSize: 18,
    ),
  ),
  tabBarTheme: const TabBarTheme(
    labelColor: AppColors.primaryColor,
    indicator: UnderlineTabIndicator(
        borderSide: BorderSide(color: AppColors.primaryColor)),
    unselectedLabelColor: AppColors.darkGray,
  ),
  textTheme: const TextTheme(
    headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
    headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
    bodyText2: TextStyle(fontSize: 16.0),
  ),
  bottomNavigationBarTheme: const BottomNavigationBarThemeData(
      selectedItemColor: AppColors.primaryColor),
  colorScheme: ColorScheme.fromSwatch(
          primarySwatch: kPrimaryColor,
          primaryColorDark: AppColors.primaryColor,
          accentColor: AppColors.accentColor,
          backgroundColor: Colors.white,
          brightness: Brightness.light,
          errorColor: Colors.red[700])
      .copyWith(secondary: AppColors.accentColor),
);
final appDarkTheme = ThemeData(
  //scaffoldBackgroundColor: Colors.white,
  // Define the default brightness and colors.
  backgroundColor: const Color(0xFF3C3C3C),

  brightness: Brightness.dark,
  primaryColor: AppColors.primaryColor,
  platform: TargetPlatform.iOS,
  // Define the default font family.
  fontFamily: 'Lato',
  appBarTheme: const AppBarTheme(
    centerTitle: true,
    elevation: 0,
    iconTheme: IconThemeData(color: Colors.white, size: 10),
    //backgroundColor: AppColors.appBarBackground,
    titleTextStyle: TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w700,
      fontSize: 18,
    ),
    toolbarTextStyle: TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w700,
      fontSize: 18,
    ),
  ),
  // Define the default TextTheme. Use this to specify the default
  // text styling for headlines, titles, bodies of text, and more.
  textTheme: const TextTheme(
    headline1: TextStyle(
      fontSize: 72.0,
      fontWeight: FontWeight.bold,
    ),
    headline6: TextStyle(
      fontSize: 36.0,
      fontStyle: FontStyle.italic,
    ),
    bodyText2: TextStyle(
      fontSize: 16.0,
    ),
  ),
  tabBarTheme: const TabBarTheme(
    labelColor: Colors.white,
    indicator: UnderlineTabIndicator(),
    unselectedLabelColor: AppColors.darkGray,
  ),
  bottomNavigationBarTheme: const BottomNavigationBarThemeData(
      selectedItemColor: AppColors.accentColor),
  colorScheme: ColorScheme.fromSwatch(
          primarySwatch: kPrimaryColor,
          primaryColorDark: AppColors.primaryColor,
          accentColor: AppColors.accentColor,
          backgroundColor: Colors.grey,
          brightness: Brightness.dark,
          errorColor: Colors.red[700])
      .copyWith(secondary: AppColors.accentColor),
);
