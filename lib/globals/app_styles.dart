import 'package:flutter/material.dart';

class AppStyles {
  static const appBarTitleBold = TextStyle(
    fontFamily: 'SF',
    color: Colors.black,
    fontSize: 20,
    fontWeight: FontWeight.bold,
  );
  static const appBarTitleNormal = TextStyle(
    fontFamily: 'SF',
    color: Colors.white,
    fontSize: 16,
  );
}
